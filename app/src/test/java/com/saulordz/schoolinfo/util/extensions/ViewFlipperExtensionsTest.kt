package com.saulordz.schoolinfo.util.extensions

import android.view.View
import android.widget.ViewFlipper
import assertk.assertThat
import assertk.assertions.isEqualTo
import com.saulordz.schoolinfo.base.BaseActivityTest
import org.junit.Test

class ViewFlipperExtensionsTest : BaseActivityTest() {

  private val viewFlipper = ViewFlipper(application)
  private val view = View(application)

  @Test
  fun testShowView() {
    viewFlipper.addView(view)

    viewFlipper.showView(view)

    assertThat(viewFlipper.displayedChild).isEqualTo(0)
  }
}