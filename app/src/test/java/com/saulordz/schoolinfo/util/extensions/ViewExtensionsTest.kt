package com.saulordz.schoolinfo.util.extensions

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import assertk.assertThat
import assertk.assertions.isEqualTo
import com.saulordz.schoolinfo.base.BaseActivityTest
import org.junit.Test

class ViewExtensionsTest : BaseActivityTest() {

  private val view = View(application)

  @Test
  fun testMakeVisible() {
    view.visibility = GONE

    view.makeVisible()

    assertThat(view.visibility).isEqualTo(VISIBLE)
  }

  @Test
  fun testMakeGone() {
    view.visibility = VISIBLE

    view.makeGone()

    assertThat(view.visibility).isEqualTo(GONE)
  }
}