package com.saulordz.schoolinfo.util.extensions

import android.widget.TextView
import assertk.assertThat
import assertk.assertions.isEqualTo
import com.saulordz.schoolinfo.R
import com.saulordz.schoolinfo.base.BaseActivityTest
import org.junit.Test

class TextViewExtensionsTest : BaseActivityTest() {

  private val view = TextView(application)
  @Test
  fun testSetText() {
    val expected = application.getString(R.string.school_details_math_score, TEST_TEXT)
    view.text = ""

    view.setText(R.string.school_details_math_score, TEST_TEXT)

    assertThat(view.text).isEqualTo(expected)
  }

  private companion object {
    private const val TEST_TEXT = "Text"
  }

}