package com.saulordz.schoolinfo.testutils

import com.saulordz.schoolinfo.rx.SchedulerComposer
import com.saulordz.schoolinfo.rx.SchedulerProvider
import io.reactivex.schedulers.Schedulers

object TestUtils {

  internal val schedulerComposer = SchedulerComposer(object : SchedulerProvider {
    override val io = Schedulers.trampoline()
    override val ui = Schedulers.trampoline()
  })

}