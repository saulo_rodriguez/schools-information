package com.saulordz.schoolinfo.data

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.saulordz.schoolinfo.data.model.SchoolInformation
import com.saulordz.schoolinfo.data.model.SchoolSat
import com.saulordz.schoolinfo.data.remote.SchoolInformationService
import com.saulordz.schoolinfo.data.repository.SchoolInformationRepository
import com.saulordz.schoolinfo.data.repository.SchoolInformationRepository.Companion.ERROR_RETRIEVING_SCHOOLS_INFORMATION
import com.saulordz.schoolinfo.data.repository.SchoolInformationRepository.Companion.ERROR_RETRIEVING_SCHOOLS_SAT
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Test
import retrofit2.Response
import java.net.HttpURLConnection.HTTP_BAD_REQUEST

class SchoolInformationRepositoryTest {

  private val mockSchoolSat = mock<SchoolSat>()
  private val mockSchoolInformation = mock<SchoolInformation>()
  private val mockSchoolInformationService = mock<SchoolInformationService>()

  private val repository = SchoolInformationRepository(mockSchoolInformationService)

  @Test
  fun testSingleGetSchoolsInformation() {
    doReturn(Single.just<Response<List<SchoolInformation>>>(Response.success(listOf(mockSchoolInformation))))
      .whenever(mockSchoolInformationService).getSchoolsInformation()
    val observer = TestObserver<List<SchoolInformation>>()

    repository.singleGetSchoolsInformation().subscribe(observer)

    verify(mockSchoolInformationService).getSchoolsInformation()
    observer.assertValue(listOf(mockSchoolInformation))
    observer.assertComplete()

  }

  @Test
  fun testSingleGetSchoolsInformationWithErrorCode() {
    doReturn(Single.just<Response<Void>>(Response.error(HTTP_BAD_REQUEST, emptyResponseBody)))
      .whenever(mockSchoolInformationService).getSchoolsInformation()
    val observer = TestObserver<List<SchoolInformation>>()

    repository.singleGetSchoolsInformation().subscribe(observer)

    verify(mockSchoolInformationService).getSchoolsInformation()
    observer.assertError {
      it is IllegalStateException
          && it.message == ERROR_RETRIEVING_SCHOOLS_INFORMATION
    }
  }

  @Test
  fun testSingleGetSchoolsSats() {
    doReturn(Single.just<Response<List<SchoolSat>>>(Response.success(listOf(mockSchoolSat))))
      .whenever(mockSchoolInformationService).getSchoolsSats()
    val observer = TestObserver<List<SchoolSat>>()

    repository.singleGetSchoolsSats().subscribe(observer)

    verify(mockSchoolInformationService).getSchoolsSats()
    observer.assertValue(listOf(mockSchoolSat))
    observer.assertComplete()
  }

  @Test
  fun testSingleGetSchoolsSatsWithErrorCode() {
    doReturn(Single.just<Response<Void>>(Response.error(HTTP_BAD_REQUEST, emptyResponseBody)))
      .whenever(mockSchoolInformationService).getSchoolsSats()
    val observer = TestObserver<List<SchoolSat>>()

    repository.singleGetSchoolsSats().subscribe(observer)

    verify(mockSchoolInformationService).getSchoolsSats()
    observer.assertError {
      it is IllegalStateException
          && it.message == ERROR_RETRIEVING_SCHOOLS_SAT
    }
  }

  private companion object {
    private val jsonMediaType = "application/json; charset=utf-8".toMediaTypeOrNull()
    private val emptyResponseBody = "{}".toResponseBody(jsonMediaType)
  }
}