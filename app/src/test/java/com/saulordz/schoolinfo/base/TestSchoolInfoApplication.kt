package com.saulordz.schoolinfo.base

import com.saulordz.schoolinfo.SchoolInfoApplication
import org.robolectric.shadows.ShadowLog
import toothpick.config.Module
import toothpick.smoothie.module.SmoothieApplicationModule

class TestSchoolInfoApplication : SchoolInfoApplication() {

  override val modules: Array<Module>
    get() = arrayOf(SmoothieApplicationModule(this))
}
