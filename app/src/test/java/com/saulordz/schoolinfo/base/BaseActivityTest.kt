package com.saulordz.schoolinfo.base

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.robolectric.Robolectric
import toothpick.Toothpick
import toothpick.testing.ToothPickRule

abstract class BaseActivityTest : BaseRobolectricTest() {

  @Rule
  @JvmField val activityScopeToothpickRule: ToothPickRule = ToothPickRule(this)

  @Before
  fun setUp() {
    activityScopeToothpickRule.inject(this)
  }

  @After
  fun cleanUp() {
    Toothpick.reset()
  }

  inline fun <reified A : AppCompatActivity> withActivity(intent: Intent? = null, action: ((A) -> Unit)) {
    val controller = Robolectric.buildActivity(A::class.java, intent)
    val activity = controller.get()
    activityScopeToothpickRule.setScopeName(activity)
    try {
      controller.create().start().resume().visible()
      action(activity)
    } finally {
      activity?.finish()
      try {
        controller.destroy()
      } catch (e: IllegalStateException) {
        throw e
      }
    }
  }
}
