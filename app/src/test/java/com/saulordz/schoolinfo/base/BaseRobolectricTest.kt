package com.saulordz.schoolinfo.base

import android.os.Build
import com.saulordz.schoolinfo.di.Scopes
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnit
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import toothpick.testing.ToothPickRule

@RunWith(RobolectricTestRunner::class)
@Config(
  application = TestSchoolInfoApplication::class,
  minSdk = Build.VERSION_CODES.O_MR1,
  maxSdk = Build.VERSION_CODES.O_MR1
)
abstract class BaseRobolectricTest {

  @Rule @JvmField val mockitoRule = MockitoJUnit.rule()
  @Rule @JvmField val appScopeToothpickRule: ToothPickRule = ToothPickRule(this, Scopes.AppScope::class.java)

  val application: TestSchoolInfoApplication
    get() = RuntimeEnvironment.application as TestSchoolInfoApplication
}
