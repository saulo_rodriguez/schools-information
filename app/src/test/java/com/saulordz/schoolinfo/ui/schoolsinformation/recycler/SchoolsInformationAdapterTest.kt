package com.saulordz.schoolinfo.ui.schoolsinformation.recycler

import android.widget.FrameLayout
import assertk.assertThat
import assertk.assertions.isInstanceOf
import assertk.assertions.isNotNull
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.saulordz.schoolinfo.base.BaseActivityTest
import com.saulordz.schoolinfo.data.model.SchoolInformation
import org.junit.Test

class SchoolsInformationAdapterTest : BaseActivityTest() {

  private val mockSchoolInformation = mock<SchoolInformation>()
  private val mockViewHolder = mock<SchoolsInformationViewHolder>()

  private val adapter = SchoolsInformationAdapter {}

  @Test
  fun testOnCreateViewHolder() {
    val viewHolder = adapter.onCreateViewHolder(FrameLayout(application), 0)

    assertThat(viewHolder).isNotNull()
    assertThat(viewHolder).isInstanceOf(SchoolsInformationViewHolder::class.java)
  }

  @Test
  fun testOnBindViewHolder() {
    adapter.submitList(listOf(mockSchoolInformation))

    adapter.onBindViewHolder(mockViewHolder, 0)

    verify(mockViewHolder).setSchoolInformation(mockSchoolInformation)
    verifyNoMoreInteractions(mockViewHolder)
  }
}