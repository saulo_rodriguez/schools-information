package com.saulordz.schoolinfo.ui.schooldetails

import com.nhaarman.mockito_kotlin.*
import com.saulordz.schoolinfo.data.model.SchoolSat
import com.saulordz.schoolinfo.data.repository.SchoolInformationRepository
import com.saulordz.schoolinfo.testutils.TestUtils.schedulerComposer
import com.saulordz.schoolinfo.ui.schooldetails.SchoolDetailsPresenter.Companion.MESSAGE_EMPTY_MATH_SCORE
import com.saulordz.schoolinfo.ui.schooldetails.SchoolDetailsPresenter.Companion.MESSAGE_EMPTY_READING_SCORE
import com.saulordz.schoolinfo.ui.schooldetails.SchoolDetailsPresenter.Companion.MESSAGE_EMPTY_WRITING_SCORE
import com.saulordz.schoolinfo.ui.schooldetails.SchoolDetailsPresenter.Companion.MESSAGE_SAT_NOT_FOUND_ERROR
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class SchoolDetailsPresenterTest {

  private val mockSchoolSat = mock<SchoolSat> {
    on { schoolId } doReturn SCHOOL_ID
    on { schoolName } doReturn SCHOOL_NAME
    on { satMathAverageScore } doReturn MATH_SCORE
    on { satReadingAverageScore } doReturn READING_SCORE
    on { satWritingAverageScore } doReturn WRITING_SCORE
  }
  private val mockSchoolInformationRepository = mock<SchoolInformationRepository> {
    on { singleGetSchoolsSats() } doReturn Single.just(listOf(mockSchoolSat))
  }
  private val mockView = mock<SchoolDetailsContract.View>()

  private val presenter = SchoolDetailsPresenter(schedulerComposer, mockSchoolInformationRepository)

  @Before
  fun init() = presenter.attachView(mockView)

  @Test
  fun testInitializeWithSchoolInResults() {
    presenter.fetchSchoolSats(SCHOOL_ID)

    verifyCommonInitializeInteractions()
    verify(mockView).showResult()
    verify(mockView).setSchoolName(SCHOOL_NAME)
    verify(mockView).setMathSatAverageScore(MATH_SCORE)
    verify(mockView).setReadingSatAverageScore(READING_SCORE)
    verify(mockView).setWritingSatAverageScore(WRITING_SCORE)
    verify(mockSchoolSat).schoolName
    verify(mockSchoolSat).satMathAverageScore
    verify(mockSchoolSat).satReadingAverageScore
    verify(mockSchoolSat).satWritingAverageScore
    verifyNoMoreInteractions(mockView, mockSchoolInformationRepository, mockSchoolSat)
  }

  @Test
  fun testInitializeWithSchoolInResultsAndEmptySatInfo() {
    doReturn(null).whenever(mockSchoolSat).schoolName
    doReturn(null).whenever(mockSchoolSat).satMathAverageScore
    doReturn(null).whenever(mockSchoolSat).satReadingAverageScore
    doReturn(null).whenever(mockSchoolSat).satWritingAverageScore

    presenter.fetchSchoolSats(SCHOOL_ID)

    verifyCommonInitializeInteractions()
    verify(mockView).showResult()
    verify(mockView).setSchoolName("")
    verify(mockView).setMathSatAverageScore(MESSAGE_EMPTY_MATH_SCORE)
    verify(mockView).setReadingSatAverageScore(MESSAGE_EMPTY_READING_SCORE)
    verify(mockView).setWritingSatAverageScore(MESSAGE_EMPTY_WRITING_SCORE)
    verify(mockSchoolSat).schoolName
    verify(mockSchoolSat).satMathAverageScore
    verify(mockSchoolSat).satReadingAverageScore
    verify(mockSchoolSat).satWritingAverageScore
    verifyNoMoreInteractions(mockView, mockSchoolInformationRepository, mockSchoolSat)
  }

  @Test
  fun testInitializeWithSchoolNotFound() {
    presenter.fetchSchoolSats("")

    verifyCommonInitializeInteractions()
    verify(mockView).showEmpty()
    verify(mockView).showError(MESSAGE_SAT_NOT_FOUND_ERROR)
    verifyNoMoreInteractions(mockView, mockSchoolInformationRepository, mockSchoolSat)
  }

  private fun verifyCommonInitializeInteractions() {
    verify(mockView).showProgress()
    verify(mockView, atLeastOnce()).hideProgress()
    verify(mockSchoolInformationRepository).singleGetSchoolsSats()
    verify(mockSchoolSat).schoolId
  }

  private companion object {
    private const val MATH_SCORE = "MathScore"
    private const val READING_SCORE = "ReadingScore"
    private const val WRITING_SCORE = "WritingScore"
    private const val SCHOOL_NAME = "SchoolName"
    private const val SCHOOL_ID = "MMBNP13"
  }
}