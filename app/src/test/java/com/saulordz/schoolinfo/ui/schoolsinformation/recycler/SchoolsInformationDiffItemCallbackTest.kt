package com.saulordz.schoolinfo.ui.schoolsinformation.recycler

import assertk.assertThat
import assertk.assertions.isFalse
import assertk.assertions.isTrue
import com.saulordz.schoolinfo.data.model.SchoolInformation
import org.junit.Test

class SchoolsInformationDiffItemCallbackTest {

  private val diffItem = SchoolsInformationDiffItemCallback()

  @Test
  fun testAreItemsTheSameWithSameValues() {
    val schoolInformation = SchoolInformation(schoolId = SCHOOL_ID)

    val actual = diffItem.areItemsTheSame(schoolInformation, schoolInformation)

    assertThat(actual).isTrue()
  }

  @Test
  fun testAreItemsTheSameWithDifferentValues() {
    val oldSchoolInformation = SchoolInformation(schoolId = SCHOOL_ID)
    val newSchoolInformation = SchoolInformation()

    val actual = diffItem.areItemsTheSame(oldSchoolInformation, newSchoolInformation)

    assertThat(actual).isFalse()
  }

  @Test
  fun testAreContentsTheSameWithSameValues() {
    val schoolInformation = SchoolInformation(schoolId = SCHOOL_ID)

    val actual = diffItem.areContentsTheSame(schoolInformation, schoolInformation)

    assertThat(actual).isTrue()
  }

  @Test
  fun testAreContentsTheSameWithDifferentValues() {
    val oldSchoolInformation = SchoolInformation(schoolId = SCHOOL_ID)
    val newSchoolInformation = SchoolInformation(schoolId = SCHOOL_ID, schoolName = SCHOOL_NAME)

    val actual = diffItem.areContentsTheSame(oldSchoolInformation, newSchoolInformation)

    assertThat(actual).isFalse()
  }

  private companion object {
    private const val SCHOOL_ID = "Msa54r"
    private const val SCHOOL_NAME = "SchoolName"
  }

}