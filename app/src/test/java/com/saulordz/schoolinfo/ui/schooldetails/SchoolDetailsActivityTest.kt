package com.saulordz.schoolinfo.ui.schooldetails

import android.content.Intent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ViewFlipper
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isTrue
import com.nhaarman.mockito_kotlin.verify
import com.saulordz.schoolinfo.R
import com.saulordz.schoolinfo.base.BaseActivityTest
import com.saulordz.schoolinfo.ui.schoolsinformation.SchoolsInformationActivity
import com.saulordz.schoolinfo.util.extensions.makeGone
import com.saulordz.schoolinfo.util.extensions.makeVisible
import com.saulordz.schoolinfo.util.extensions.showView
import kotlinx.android.synthetic.main.activity_school_details.*
import kotlinx.android.synthetic.main.include_toolbar.*
import org.junit.Test
import org.mockito.Mock

class SchoolDetailsActivityTest : BaseActivityTest() {

  @Mock
  lateinit var mockPresenter: SchoolDetailsContract.Presenter

  @Test
  fun testOnCreateInteractions() = withActivity<SchoolDetailsActivity>(createStartingIntent()) {
    verify(mockPresenter).fetchSchoolSats(SCHOOL_ID)
  }

  @Test
  fun testOnCreateInteractionsWithEmptyBundle() = withActivity<SchoolDetailsActivity>() {
    verify(mockPresenter).fetchSchoolSats("")
  }

  @Test
  fun testShowProgress() = withActivity<SchoolDetailsActivity>() {
    it.a_details_progress.makeGone()

    it.showProgress()

    assertThat(it.a_details_progress.visibility).isEqualTo(VISIBLE)
  }

  @Test
  fun testHideProgress() = withActivity<SchoolDetailsActivity> {
    it.a_details_progress.makeVisible()

    it.hideProgress()

    assertThat(it.a_details_progress.visibility).isEqualTo(GONE)
  }

  @Test
  fun testSetMathSatAverageScore() = withActivity<SchoolDetailsActivity> {
    it.a_details_math_score.text = ""

    it.setMathSatAverageScore(MATH_SCORE)

    val expectedText = application.getString(R.string.school_details_math_score, MATH_SCORE)
    assertThat(it.a_details_math_score.text).isEqualTo(expectedText)
  }

  @Test
  fun testSetReadingSatAverageScore() = withActivity<SchoolDetailsActivity> {
    it.a_details_reading_score.text = ""

    it.setReadingSatAverageScore(READING_SCORE)

    val expectedText = application.getString(R.string.school_details_reading_score, READING_SCORE)
    assertThat(it.a_details_reading_score.text).isEqualTo(expectedText)
  }

  @Test
  fun testSetWritingSatAverageScore() = withActivity<SchoolDetailsActivity> {
    it.a_details_writing_score.text = ""

    it.setWritingSatAverageScore(WRITING_SCORE)

    val expectedText = application.getString(R.string.school_details_writing_score, WRITING_SCORE)
    assertThat(it.a_details_writing_score.text).isEqualTo(expectedText)
  }

  @Test
  fun testSetSchoolName() = withActivity<SchoolDetailsActivity> {
    it.i_toolbar.title = ""

    it.setSchoolName(SCHOOL_NAME)

    assertThat(it.i_toolbar.title).isEqualTo(SCHOOL_NAME)
  }

  @Test
  fun testShowEmpty() = withActivity<SchoolDetailsActivity> {
    it.a_details_view_flipper.showView(it.a_details_result)

    it.showEmpty()

    assertThat(it.a_details_view_flipper.isDisplayed(it.a_details_empty)).isTrue()
  }

  @Test
  fun testShowResult() = withActivity<SchoolDetailsActivity> { }

  private fun createStartingIntent() =
    Intent().putExtra(SchoolsInformationActivity.KEY_SCHOOL_ID, SCHOOL_ID)

  private fun ViewFlipper.isDisplayed(view: View) = indexOfChild(view) == displayedChild

  private companion object {
    private const val SCHOOL_ID = "Nvmsj8"
    private const val MATH_SCORE = "MathScore"
    private const val READING_SCORE = "ReadingScore"
    private const val WRITING_SCORE = "WritingScore"
    private const val SCHOOL_NAME = "SchoolName"
  }
}