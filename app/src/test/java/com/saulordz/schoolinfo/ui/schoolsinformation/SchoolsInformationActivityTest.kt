package com.saulordz.schoolinfo.ui.schoolsinformation

import android.content.ComponentName
import android.view.View.GONE
import android.view.View.VISIBLE
import assertk.assertThat
import assertk.assertions.isEqualTo
import com.nhaarman.mockito_kotlin.verify
import com.saulordz.schoolinfo.base.BaseActivityTest
import com.saulordz.schoolinfo.ui.schooldetails.SchoolDetailsActivity
import com.saulordz.schoolinfo.ui.schoolsinformation.SchoolsInformationActivity.Companion.KEY_SCHOOL_ID
import com.saulordz.schoolinfo.util.extensions.makeGone
import com.saulordz.schoolinfo.util.extensions.makeVisible
import kotlinx.android.synthetic.main.activity_schools_information.*
import org.junit.Test
import org.mockito.Mock
import org.robolectric.shadows.ShadowApplication

class SchoolsInformationActivityTest : BaseActivityTest() {

  @Mock
  lateinit var mockPresenter: SchoolsInformationContract.Presenter

  @Test
  fun testLoadButtonClick() = withActivity<SchoolsInformationActivity> {
    it.a_schools_information_load_button.performClick()

    verify(mockPresenter).loadSchoolsInformation()
  }

  @Test
  fun testNavigateSchoolDetails() = withActivity<SchoolsInformationActivity> {
    it.navigateSchoolDetails(SCHOOL_ID)

    val intent = ShadowApplication.getInstance().nextStartedActivity
    assertThat(intent.component).isEqualTo(ComponentName(it, SchoolDetailsActivity::class.java))
    assertThat(intent.getStringExtra(KEY_SCHOOL_ID)).isEqualTo(SCHOOL_ID)
  }

  @Test
  fun testShowProgress() = withActivity<SchoolsInformationActivity> {
    it.a_schools_information_progress.makeGone()

    it.showProgress()

    assertThat(it.a_schools_information_progress.visibility).isEqualTo(VISIBLE)
  }

  @Test
  fun testHideProgress() = withActivity<SchoolsInformationActivity> {
    it.a_schools_information_progress.makeVisible()

    it.hideProgress()

    assertThat(it.a_schools_information_progress.visibility).isEqualTo(GONE)
  }

  @Test
  fun testOnSchoolInformationClicked() = withActivity<SchoolsInformationActivity> {
    it.onSchoolInformationClicked(SCHOOL_ID)

    verify(mockPresenter).validateSchoolInformation(SCHOOL_ID)
  }

  private companion object {
    private const val SCHOOL_ID = "NVM3012"
  }
}