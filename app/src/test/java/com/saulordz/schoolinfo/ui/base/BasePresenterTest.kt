package com.saulordz.schoolinfo.ui.base

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.isNull
import assertk.assertions.isTrue
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.saulordz.schoolinfo.base.BaseContract
import com.saulordz.schoolinfo.base.BasePresenter
import com.saulordz.schoolinfo.rx.SchedulerComposer
import com.saulordz.schoolinfo.testutils.TestUtils.schedulerComposer
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test

class BasePresenterTest {

  private val mockView = mock<BaseContract.View>()

  private lateinit var presenter: ConcretePresenter

  @Before
  fun setUp() {
    presenter = ConcretePresenter(schedulerComposer)
    presenter.attachView(mockView)
  }

  @Test
  fun testAttachView() {
    presenter.detachView()

    presenter.attachView(mockView)

    assertThat(presenter.compositeDisposable).isNotNull()
  }

  @Test
  fun testDetachView() {
    val disposable = Completable.complete().subscribe()
    presenter.compositeDisposable?.add(disposable)

    presenter.detachView()

    assertThat(disposable.isDisposed).isTrue()
    assertThat(presenter.compositeDisposable).isNull()
  }

  @Test
  fun testAddDisposable() {
    val disposable = Completable.complete().subscribe()
    val actionToDispose = { disposable }

    presenter.addDisposable(actionToDispose)

    verify(mockView).showProgress()
    assertThat(presenter.compositeDisposable?.remove(disposable)).isEqualTo(true)
    verifyNoMoreInteractions(mockView)
  }

  @Test
  fun testOnError() {
    presenter.onError(IllegalStateException(ERROR_MESSAGE))

    verify(mockView).hideProgress()
    verify(mockView).showError(ERROR_MESSAGE)
    verifyNoMoreInteractions(mockView)
  }

  private companion object {
    private const val ERROR_MESSAGE = "ErrorMessage"
  }

  private class ConcretePresenter(
    schedulerComposer: SchedulerComposer
  ) : BasePresenter<BaseContract.View>(schedulerComposer)
}