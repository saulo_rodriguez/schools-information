package com.saulordz.schoolinfo.ui.schoolsinformation

import com.nhaarman.mockito_kotlin.*
import com.saulordz.schoolinfo.data.model.SchoolInformation
import com.saulordz.schoolinfo.data.repository.SchoolInformationRepository
import com.saulordz.schoolinfo.testutils.TestUtils.schedulerComposer
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class SchoolsInformationPresenterTest {

  private val mockSchoolInformation = mock<SchoolInformation>()
  private val mockSchoolInformationRepository = mock<SchoolInformationRepository> {
    on { singleGetSchoolsInformation() } doReturn Single.just(listOf(mockSchoolInformation))
  }
  private val mockView = mock<SchoolsInformationContract.View>()

  private val presenter = SchoolsInformationPresenter(schedulerComposer, mockSchoolInformationRepository)

  @Before
  fun init() = presenter.attachView(mockView)

  @Test
  fun testLoadSchoolsInformation() {
    presenter.loadSchoolsInformation()

    verify(mockView).showProgress()
    verify(mockView).hideProgress()
    verify(mockView).schoolsInformation = listOf(mockSchoolInformation)
    verify(mockSchoolInformationRepository).singleGetSchoolsInformation()
    verifyNoMoreInteractions(mockView, mockSchoolInformationRepository)
  }

  @Test
  fun testValidateSchoolInformation() {
    presenter.validateSchoolInformation(SCHOOL_ID)

    verify(mockView).navigateSchoolDetails(SCHOOL_ID)
    verifyNoMoreInteractions(mockView)
  }

  @Test
  fun testValidateSchoolInformationWithNullId() {
    presenter.validateSchoolInformation(null)

    verify(mockView).hideProgress()
    verify(mockView).showError(any())
    verifyNoMoreInteractions(mockView)
  }

  private companion object {
    private const val SCHOOL_ID = "M5003A"
  }
}