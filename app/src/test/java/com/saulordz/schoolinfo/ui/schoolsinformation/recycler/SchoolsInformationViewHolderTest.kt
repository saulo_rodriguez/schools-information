package com.saulordz.schoolinfo.ui.schoolsinformation.recycler

import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isTrue
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.saulordz.schoolinfo.R
import com.saulordz.schoolinfo.base.BaseActivityTest
import com.saulordz.schoolinfo.data.model.SchoolInformation
import kotlinx.android.synthetic.main.item_school_information.view.*
import org.junit.Before
import org.junit.Test

class SchoolsInformationViewHolderTest : BaseActivityTest() {

  private val mockSchoolInformation = mock<SchoolInformation> {
    on { schoolId } doReturn SCHOOL_ID
    on { schoolName } doReturn SCHOOL_NAME
  }

  private lateinit var itemView: View

  @Before
  fun initialize() {
    itemView = LayoutInflater.from(application).inflate(R.layout.item_school_information, FrameLayout(application), false)
  }

  @Test
  fun testSetSchoolInformation() = with(itemView) {
    val holder = SchoolsInformationViewHolder(itemView) {}

    holder.setSchoolInformation(mockSchoolInformation)

    assertThat(i_school_id.text).isEqualTo(SCHOOL_ID)
    assertThat(i_school_name.text).isEqualTo(SCHOOL_NAME)
  }

  @Test
  fun testClickViewHolder() = with(itemView) {
    var wasCalled = false
    val holder = SchoolsInformationViewHolder(itemView) { wasCalled = true }
    holder.setSchoolInformation(SchoolInformation())

    performClick()

    assertThat(wasCalled).isTrue()
  }

  private companion object {
    private const val SCHOOL_ID = "Nvmsj8"
    private const val SCHOOL_NAME = "SchoolName"
  }
}