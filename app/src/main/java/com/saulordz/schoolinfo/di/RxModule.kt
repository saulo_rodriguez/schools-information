package com.saulordz.schoolinfo.di

import com.saulordz.schoolinfo.rx.ApplicationSchedulerProvider
import com.saulordz.schoolinfo.rx.SchedulerProvider
import toothpick.config.Module
import javax.inject.Inject

class RxModule @Inject constructor() : Module() {
  init {
    bind(SchedulerProvider::class.java).to(ApplicationSchedulerProvider::class.java)
  }
}