package com.saulordz.schoolinfo.di.remote

import com.saulordz.schoolinfo.data.remote.SchoolInformationService
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Provider

class SchoolInformationServiceProvider @Inject constructor(
  private val retrofit: Retrofit
) : Provider<SchoolInformationService> {

  override fun get(): SchoolInformationService =
    retrofit.create(SchoolInformationService::class.java)

}