package com.saulordz.schoolinfo.base

import android.os.Bundle
import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.saulordz.schoolinfo.R
import com.saulordz.schoolinfo.di.Scopes
import toothpick.Scope
import toothpick.Toothpick
import toothpick.smoothie.module.SmoothieActivityModule

abstract class BaseActivity<V : BaseContract.View, P : MvpPresenter<V>> :
  MvpActivity<V, P>(), MvpView, BaseContract.View {

  private lateinit var scope: Scope

  override fun onCreate(savedInstanceState: Bundle?) {
    initializeToothpick()
    super.onCreate(savedInstanceState)
  }

  override fun onDestroy() {
    Toothpick.closeScope(this)

    super.onDestroy()
  }

  override fun showError(message: CharSequence) {
    val rootView = findViewById<View>(R.id.a_base_coordinator)
    rootView?.let {
      Snackbar.make(it, message, Snackbar.LENGTH_INDEFINITE).show()
    }
  }

  abstract fun addModules(scope: Scope): Scope

  private fun initializeToothpick() {
    scope = Toothpick.openScopes(Scopes.AppScope::class.java, this)
    scope.installModules(SmoothieActivityModule(this))
    addModules(scope)
    Toothpick.inject(this, scope)
  }

}