package com.saulordz.schoolinfo.base

import androidx.annotation.VisibleForTesting
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.saulordz.schoolinfo.rx.SchedulerComposer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<V : BaseContract.View>(
  open val schedulerComposer: SchedulerComposer
) : MvpBasePresenter<V>(), MvpPresenter<V>, BaseContract.Presenter<V> {

  @VisibleForTesting internal var compositeDisposable: CompositeDisposable? = null

  override fun attachView(view: V) {
    compositeDisposable = CompositeDisposable()
    super.attachView(view)
  }

  override fun detachView() {
    compositeDisposable?.dispose()
    compositeDisposable = null
    super.detachView()
  }

  internal fun addDisposable(actionToDispose: () -> Disposable) = ifViewAttached { view ->
    view.showProgress()
    compositeDisposable?.add(actionToDispose())
  }

  override fun onError(throwable: Throwable) = ifViewAttached { view ->
    view.hideProgress()
    view.showError(throwable.message.orEmpty())
  }

}