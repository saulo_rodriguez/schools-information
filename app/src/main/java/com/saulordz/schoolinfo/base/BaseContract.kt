package com.saulordz.schoolinfo.base

import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView

interface BaseContract {

  interface View : MvpView {
    fun showProgress()
    fun hideProgress()
    fun showError(message: CharSequence)
  }

  interface Presenter<V : View> : MvpPresenter<V> {
    fun onError(throwable: Throwable)
  }
}