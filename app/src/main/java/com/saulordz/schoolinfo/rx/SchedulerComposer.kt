package com.saulordz.schoolinfo.rx

import io.reactivex.SingleTransformer
import javax.inject.Inject

class SchedulerComposer @Inject constructor(schedulerProvider: SchedulerProvider) {

  private val singleIoUiComposer = SingleComposer(schedulerProvider.io, schedulerProvider.ui)

  @Suppress("UNCHECKED_CAST")
  fun <T> singleIoUiComposer() = singleIoUiComposer as SingleTransformer<T, T>

}