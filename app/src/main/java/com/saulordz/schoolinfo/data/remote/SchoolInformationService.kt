package com.saulordz.schoolinfo.data.remote

import com.saulordz.schoolinfo.data.model.SchoolInformation
import com.saulordz.schoolinfo.data.model.SchoolSat
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface SchoolInformationService {

  @GET("s3k6-pzi2.json")
  fun getSchoolsInformation(): Single<Response<List<SchoolInformation>>>

  @GET("f9bf-2cp4.json")
  fun getSchoolsSats(): Single<Response<List<SchoolSat>>>

}