package com.saulordz.schoolinfo.data.repository

import androidx.annotation.VisibleForTesting
import com.saulordz.schoolinfo.data.model.SchoolInformation
import com.saulordz.schoolinfo.data.model.SchoolSat
import com.saulordz.schoolinfo.data.remote.SchoolInformationService
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SchoolInformationRepository @Inject constructor(
  private val schoolInformationService: SchoolInformationService
) {

  fun singleGetSchoolsInformation(): Single<List<SchoolInformation>> =
    schoolInformationService.getSchoolsInformation()
      .filter { it.isSuccessful && it.body() != null }
      .switchIfEmpty(Single.error(IllegalStateException(ERROR_RETRIEVING_SCHOOLS_INFORMATION)))
      .map { it.body() }


  fun singleGetSchoolsSats(): Single<List<SchoolSat>> =
    schoolInformationService.getSchoolsSats()
      .filter { it.isSuccessful && it.body() != null }
      .switchIfEmpty(Single.error(IllegalStateException(ERROR_RETRIEVING_SCHOOLS_SAT)))
      .map { it.body() }

  @VisibleForTesting
  internal companion object {
    @VisibleForTesting internal const val ERROR_RETRIEVING_SCHOOLS_INFORMATION = "Error retrieving schools information"
    @VisibleForTesting internal const val ERROR_RETRIEVING_SCHOOLS_SAT = "Error retrieving schools SATs"
  }
}
