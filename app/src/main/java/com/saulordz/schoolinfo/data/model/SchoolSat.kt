package com.saulordz.schoolinfo.data.model

import com.squareup.moshi.Json

data class SchoolSat(
  @field:Json(name = "dbn") val schoolId: String? = null,
  @field:Json(name = "num_of_sat_test_takers") val satTestTakers: String? = null,
  @field:Json(name = "sat_critical_reading_avg_score") val satReadingAverageScore: String? = null,
  @field:Json(name = "sat_math_avg_score") val satMathAverageScore: String? = null,
  @field:Json(name = "sat_writing_avg_score") val satWritingAverageScore: String? = null,
  @field:Json(name = "school_name") val schoolName: String? = null
)