package com.saulordz.schoolinfo.ui.schoolsinformation

import android.content.Intent
import android.os.Bundle
import androidx.annotation.VisibleForTesting
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import com.saulordz.schoolinfo.R
import com.saulordz.schoolinfo.base.BaseActivity
import com.saulordz.schoolinfo.data.model.SchoolInformation
import com.saulordz.schoolinfo.ui.schoolsinformation.recycler.SchoolsInformationAdapter
import com.saulordz.schoolinfo.ui.schooldetails.SchoolDetailsActivity
import com.saulordz.schoolinfo.util.extensions.makeGone
import com.saulordz.schoolinfo.util.extensions.makeVisible
import kotlinx.android.synthetic.main.activity_schools_information.*
import toothpick.Scope
import javax.inject.Inject

class SchoolsInformationActivity : BaseActivity<SchoolsInformationContract.View, SchoolsInformationContract.Presenter>(),
  SchoolsInformationContract.View {

  @Inject
  lateinit var schoolsInformationPresenter: SchoolsInformationContract.Presenter

  override var schoolsInformation: List<SchoolInformation>? = null
    set(value) = schoolsInformationAdapter.submitList(value)

  private val schoolsInformationAdapter: SchoolsInformationAdapter by lazy { SchoolsInformationAdapter(::onSchoolInformationClicked) }

  override fun addModules(scope: Scope): Scope = scope.installModules(SchoolsInformationModule())

  override fun createPresenter(): SchoolsInformationContract.Presenter = schoolsInformationPresenter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_schools_information)

    initViews()
  }

  override fun showProgress() = a_schools_information_progress.makeVisible()

  override fun hideProgress() = a_schools_information_progress.makeGone()

  override fun navigateSchoolDetails(schoolId: String) {
    val intent = Intent(this, SchoolDetailsActivity::class.java)
      .putExtra(KEY_SCHOOL_ID, schoolId)

    startActivity(intent)
  }

  @VisibleForTesting
  internal fun onSchoolInformationClicked(schoolId: String?) = presenter.validateSchoolInformation(schoolId)

  private fun initViews() {
    a_schools_information_recycler_view.adapter = schoolsInformationAdapter
    a_schools_information_recycler_view.addItemDecoration(DividerItemDecoration(this, VERTICAL))
    a_schools_information_load_button.setOnClickListener {
      presenter.loadSchoolsInformation()
    }
  }

  companion object {
    const val KEY_SCHOOL_ID = "key_school_id"
  }
}
