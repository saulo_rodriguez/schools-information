package com.saulordz.schoolinfo.ui.schooldetails

import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.saulordz.schoolinfo.base.BaseContract

interface SchoolDetailsContract {

  interface View : BaseContract.View {
    fun setSchoolName(schoolName: String)
    fun setMathSatAverageScore(mathSat: String)
    fun setReadingSatAverageScore(readingSat: String)
    fun setWritingSatAverageScore(writingSat: String)
    fun showResult()
    fun showEmpty()
  }

  interface Presenter : MvpPresenter<View> {
    fun fetchSchoolSats(schoolId: String)
  }
}