package com.saulordz.schoolinfo.ui.schooldetails

import toothpick.config.Module

class SchoolDetailsModule : Module() {
  init {
    bind(SchoolDetailsContract.Presenter::class.java).to(SchoolDetailsPresenter::class.java)
  }
}