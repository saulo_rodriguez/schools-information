package com.saulordz.schoolinfo.ui.schoolsinformation.recycler

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.saulordz.schoolinfo.data.model.SchoolInformation
import kotlinx.android.synthetic.main.item_school_information.view.*

class SchoolsInformationViewHolder(
  private val view: View,
  private val onSchoolClicked: (String?) -> Unit
) : RecyclerView.ViewHolder(view) {

  fun setSchoolInformation(schoolInformation: SchoolInformation) = with(view) {
    setOnClickListener {
      onSchoolClicked(schoolInformation.schoolId)
    }
    i_school_id.text = schoolInformation.schoolId
    i_school_name.text = schoolInformation.schoolName
  }
}