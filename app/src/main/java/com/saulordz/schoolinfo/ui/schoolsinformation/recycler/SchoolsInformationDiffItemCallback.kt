package com.saulordz.schoolinfo.ui.schoolsinformation.recycler

import androidx.recyclerview.widget.DiffUtil
import com.saulordz.schoolinfo.data.model.SchoolInformation

class SchoolsInformationDiffItemCallback : DiffUtil.ItemCallback<SchoolInformation>() {

  override fun areItemsTheSame(oldItem: SchoolInformation, newItem: SchoolInformation) =
    oldItem.schoolId == newItem.schoolId

  override fun areContentsTheSame(oldItem: SchoolInformation, newItem: SchoolInformation) =
    oldItem == newItem

}