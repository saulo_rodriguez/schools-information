package com.saulordz.schoolinfo.ui.schooldetails

import androidx.annotation.VisibleForTesting
import com.saulordz.schoolinfo.base.BasePresenter
import com.saulordz.schoolinfo.data.model.SchoolSat
import com.saulordz.schoolinfo.data.repository.SchoolInformationRepository
import com.saulordz.schoolinfo.rx.SchedulerComposer
import javax.inject.Inject

class SchoolDetailsPresenter @Inject constructor(
  schedulerComposer: SchedulerComposer,
  private val schoolInformationRepository: SchoolInformationRepository
) : BasePresenter<SchoolDetailsContract.View>(schedulerComposer), SchoolDetailsContract.Presenter {

  override fun fetchSchoolSats(schoolId: String) = addDisposable {
    schoolInformationRepository
      .singleGetSchoolsSats()
      .compose(schedulerComposer.singleIoUiComposer())
      .subscribe({ handleFetchSchoolDetailsSuccess(schoolId, it) }) { onError(it) }
  }

  private fun handleFetchSchoolDetailsSuccess(schoolId: String, sats: List<SchoolSat>) = ifViewAttached { view ->
    view.hideProgress()
    val schoolSat = sats.find { it.schoolId.equals(schoolId, ignoreCase = true) }
    if (schoolSat != null) {
      view.showResult()
      view.setSchoolName(schoolSat.schoolName.orEmpty())
      view.setMathSatAverageScore(schoolSat.satMathAverageScore.orDefault(MESSAGE_EMPTY_MATH_SCORE))
      view.setReadingSatAverageScore(schoolSat.satReadingAverageScore.orDefault(MESSAGE_EMPTY_READING_SCORE))
      view.setWritingSatAverageScore(schoolSat.satWritingAverageScore.orDefault(MESSAGE_EMPTY_WRITING_SCORE))
    } else {
      view.showEmpty()
      onError(IllegalStateException(MESSAGE_SAT_NOT_FOUND_ERROR + schoolId))
    }
  }

  private fun String?.orDefault(defaultString: String) = this ?: defaultString

  @VisibleForTesting
  internal companion object {
    @VisibleForTesting internal const val MESSAGE_EMPTY_MATH_SCORE = "Math average score not available"
    @VisibleForTesting internal const val MESSAGE_EMPTY_READING_SCORE = "Reading average score not available"
    @VisibleForTesting internal const val MESSAGE_EMPTY_WRITING_SCORE = "Writing average score not available"
    @VisibleForTesting internal const val MESSAGE_SAT_NOT_FOUND_ERROR = "School SAT not found for "
  }
}