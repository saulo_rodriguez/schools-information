package com.saulordz.schoolinfo.ui.schooldetails

import android.os.Bundle
import com.saulordz.schoolinfo.R
import com.saulordz.schoolinfo.base.BaseActivity
import com.saulordz.schoolinfo.ui.schoolsinformation.SchoolsInformationActivity.Companion.KEY_SCHOOL_ID
import com.saulordz.schoolinfo.util.extensions.makeGone
import com.saulordz.schoolinfo.util.extensions.makeVisible
import com.saulordz.schoolinfo.util.extensions.setText
import com.saulordz.schoolinfo.util.extensions.showView
import kotlinx.android.synthetic.main.activity_school_details.*
import kotlinx.android.synthetic.main.include_toolbar.*
import toothpick.Scope
import javax.inject.Inject

class SchoolDetailsActivity : BaseActivity<SchoolDetailsContract.View, SchoolDetailsContract.Presenter>(),
  SchoolDetailsContract.View {

  @Inject
  lateinit var schoolDetailsPresenter: SchoolDetailsContract.Presenter

  private val schoolId by lazy { parseSchoolId() }

  override fun addModules(scope: Scope): Scope = scope.installModules(SchoolDetailsModule())

  override fun createPresenter() = schoolDetailsPresenter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_school_details)
    setSupportActionBar(i_toolbar)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)

    initViews()
  }

  override fun showProgress() = a_details_progress.makeVisible()

  override fun hideProgress() = a_details_progress.makeGone()

  override fun setMathSatAverageScore(mathSat: String) =
    a_details_math_score.setText(R.string.school_details_math_score, mathSat)

  override fun setReadingSatAverageScore(readingSat: String) =
    a_details_reading_score.setText(R.string.school_details_reading_score, readingSat)

  override fun setWritingSatAverageScore(writingSat: String) =
    a_details_writing_score.setText(R.string.school_details_writing_score, writingSat)

  override fun setSchoolName(schoolName: String) {
    i_toolbar.title = schoolName
  }

  override fun showEmpty() = a_details_view_flipper.showView(a_details_empty)

  override fun showResult() = a_details_view_flipper.showView(a_details_result)

  private fun initViews() {
    presenter.fetchSchoolSats(schoolId)
  }

  private fun parseSchoolId() = intent.getStringExtra(KEY_SCHOOL_ID).orEmpty()
}
