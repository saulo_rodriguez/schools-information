package com.saulordz.schoolinfo.ui.schoolsinformation

import toothpick.config.Module

class SchoolsInformationModule : Module() {
  init {
    bind(SchoolsInformationContract.Presenter::class.java).to(SchoolsInformationPresenter::class.java)
  }
}