package com.saulordz.schoolinfo.ui.schoolsinformation.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.saulordz.schoolinfo.R
import com.saulordz.schoolinfo.data.model.SchoolInformation

class SchoolsInformationAdapter(
  private val onSchoolClicked: (String?) -> Unit
) :
  ListAdapter<SchoolInformation, SchoolsInformationViewHolder>(SchoolsInformationDiffItemCallback()) {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
    SchoolsInformationViewHolder(
      parent.inflateView(),
      onSchoolClicked
    )

  override fun onBindViewHolder(holder: SchoolsInformationViewHolder, position: Int) =
    holder.setSchoolInformation(getItem(position))

  private fun ViewGroup.inflateView() = LayoutInflater.from(context)
    .inflate(R.layout.item_school_information, this, false)
}