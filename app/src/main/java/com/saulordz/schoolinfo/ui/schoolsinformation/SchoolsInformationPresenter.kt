package com.saulordz.schoolinfo.ui.schoolsinformation

import com.saulordz.schoolinfo.base.BasePresenter
import com.saulordz.schoolinfo.data.model.SchoolInformation
import com.saulordz.schoolinfo.data.repository.SchoolInformationRepository
import com.saulordz.schoolinfo.rx.SchedulerComposer
import javax.inject.Inject

class SchoolsInformationPresenter @Inject constructor(
  schedulerComposer: SchedulerComposer,
  private val schoolInformationRepository: SchoolInformationRepository
) : BasePresenter<SchoolsInformationContract.View>(schedulerComposer), SchoolsInformationContract.Presenter {

  override fun loadSchoolsInformation() = addDisposable {
    schoolInformationRepository
      .singleGetSchoolsInformation()
      .compose(schedulerComposer.singleIoUiComposer())
      .subscribe(::handleLoadSchoolsInformationSuccess) { onError(it) }
  }

  override fun validateSchoolInformation(schoolId: String?) = ifViewAttached { view ->
    if (schoolId.isNullOrBlank()) {
      onError(IllegalStateException())
    } else {
      view.navigateSchoolDetails(schoolId)
    }
  }

  private fun handleLoadSchoolsInformationSuccess(schoolsInformation: List<SchoolInformation>) = ifViewAttached { view ->
    view.hideProgress()
    view.schoolsInformation = schoolsInformation
  }
}