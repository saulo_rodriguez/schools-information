package com.saulordz.schoolinfo.ui.schoolsinformation

import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.saulordz.schoolinfo.base.BaseContract
import com.saulordz.schoolinfo.data.model.SchoolInformation

interface SchoolsInformationContract {

  interface View : BaseContract.View {
    //add show and hideprogress to base activity
    var schoolsInformation: List<SchoolInformation>?
    fun navigateSchoolDetails(schoolId: String)
  }

  interface Presenter : MvpPresenter<View> {
    fun loadSchoolsInformation()
    fun validateSchoolInformation(schoolId: String?)
  }
}