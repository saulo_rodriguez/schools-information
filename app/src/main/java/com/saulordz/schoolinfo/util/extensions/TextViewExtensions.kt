package com.saulordz.schoolinfo.util.extensions

import android.widget.TextView
import androidx.annotation.StringRes

internal fun TextView.setText(@StringRes resId: Int, vararg args: Any?) {
  val string = context.getString(resId, *args)
  text = string
}